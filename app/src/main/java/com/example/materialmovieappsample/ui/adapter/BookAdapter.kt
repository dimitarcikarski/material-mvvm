package com.example.materialmovieappsample.ui.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.materialmovieappsample.data.model.Book
import com.example.materialmovieappsample.databinding.ItemBookBinding
import java.util.ArrayList

class BookAdapter(
    private val books: MutableList<Book>,
    private val context: Context,
    private val bookClickInterface: BookClick
) : RecyclerView.Adapter<BookAdapter.BookViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): BookViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val view = ItemBookBinding.inflate(inflater, parent, false)
        return BookViewHolder(view)
    }

    override fun onBindViewHolder(holder: BookViewHolder, position: Int) {

        if (books[position] != null) {
            Glide
                .with(context)
                .load(books[position].volumeInfo.imageLinks.thumbnail)
                .centerCrop()
                .into(holder.binding.bookImg)
        }


        holder.binding.bookImg.setOnClickListener {
            bookClickInterface.onBookClick(books[position])
        }
    }

    override fun getItemCount(): Int = books.size

    class BookViewHolder(val binding: ItemBookBinding) : RecyclerView.ViewHolder(binding.root)
}
