package com.example.materialmovieappsample.ui.main

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.lifecycleScope
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.materialmovieappsample.R
import com.example.materialmovieappsample.data.model.Book
import com.example.materialmovieappsample.databinding.FragmentMainBinding
import com.example.materialmovieappsample.ui.adapter.BookAdapter
import com.example.materialmovieappsample.ui.adapter.BookClick
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import org.koin.android.viewmodel.ext.android.viewModel


class MainFragment : Fragment() , BookClick {

    private lateinit var binding : FragmentMainBinding
    private val viewModel: MainViewModel by viewModel()
    private lateinit var adapterMeals: BookAdapter
    private lateinit var adapterHealthyMeals: BookAdapter
    private lateinit var adapterHealthyLifestyle: BookAdapter

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = DataBindingUtil.inflate(inflater,R.layout.fragment_main,container,false)
        return binding.root
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

//        lifecycleScope.launch(Dispatchers.Main) {
            viewModel.fetchWellnessBooks().observe(viewLifecycleOwner, observerWellness())
            viewModel.fetchHealthyLifestyle().observe(viewLifecycleOwner, observerHealthyLifestyle())
            viewModel.fetchHealthyMealsBooks().observe(viewLifecycleOwner, observerHealthyMeals())
//        }
    }

    private fun observerWellness() : Observer<in MutableList<Book>> = Observer {
        adapterMeals = BookAdapter(it,requireActivity(),this)
        binding.healthyLifestyleRecyclerView.apply {
            layoutManager =
                LinearLayoutManager(requireContext(), LinearLayoutManager.HORIZONTAL, false)

            adapter = adapterMeals
        }
    }

    private fun observerHealthyMeals() : Observer<in MutableList<Book>> = Observer {
        adapterHealthyMeals = BookAdapter(it,requireActivity(),this)
        binding.healthyMealsRecyclerView.apply {
            layoutManager =
                LinearLayoutManager(requireContext(), LinearLayoutManager.HORIZONTAL, false)

            adapter = adapterHealthyMeals
        }

    }

    private fun observerHealthyLifestyle() : Observer<in MutableList<Book>> = Observer {
        adapterHealthyLifestyle = BookAdapter(it,requireActivity(),this)
        binding.wellnessRecyclerView.apply {
            layoutManager =
                LinearLayoutManager(requireContext(), LinearLayoutManager.HORIZONTAL, false)

            adapter = adapterHealthyLifestyle
        }
    }


    override fun onBookClick(book: Book) {

    }

    override fun onSeeAllClick(sectionType: Int, bookType: Int) {

    }


}