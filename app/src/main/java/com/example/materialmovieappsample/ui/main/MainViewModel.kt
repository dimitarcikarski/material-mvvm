package com.example.materialmovieappsample.ui.main

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.example.materialmovieappsample.data.model.Book
import com.example.materialmovieappsample.data.repository.BookRepository

class MainViewModel(private val bookRepository: BookRepository) : ViewModel(){


    fun fetchWellnessBooks() : MutableLiveData<MutableList<Book>>
    = bookRepository.getBooks("intitle:wellness")

    fun fetchHealthyMealsBooks() : MutableLiveData<MutableList<Book>>
    = bookRepository.getBooks("intitle:fit")

    fun fetchHealthyLifestyle() : MutableLiveData<MutableList<Book>>
    = bookRepository.getBooks("intitle:yoga")

}