package com.example.materialmovieappsample.ui.adapter

import com.example.materialmovieappsample.data.model.Book

interface BookClick {
    fun onBookClick(book: Book)
    fun onSeeAllClick(sectionType: Int, bookType: Int)
}