package com.example.materialmovieappsample.data.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import java.util.*

@Parcelize
class BookResponseModel(
    var kind: String,
    var totalItems: Int,
    var items: MutableList<Book>,
    var sectionType: Int
) : Parcelable