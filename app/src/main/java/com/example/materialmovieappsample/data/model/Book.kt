package com.example.materialmovieappsample.data.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
class Book(
    var id: String,
    var selfLink: String,
    var volumeInfo: BookVolumeInfo
) : Parcelable