package com.example.materialmovieappsample.data.repository

import androidx.lifecycle.MutableLiveData
import com.example.materialmovieappsample.data.api.BookService
import com.example.materialmovieappsample.data.api.RetrofitManager
import com.example.materialmovieappsample.data.model.Book
import com.example.materialmovieappsample.data.model.BookResponseModel
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class BookRepository(private val retrofitManager: RetrofitManager) {

    private val bookService: BookService =
        retrofitManager.getRetrofit().create(BookService::class.java)

    fun getBooks(
        intitle: String
    ): MutableLiveData<MutableList<Book>> {

        val booksObservable: MutableLiveData<MutableList<Book>> by lazy { MutableLiveData<MutableList<Book>>() }

        bookService.healthyLifestyle(intitle).enqueue(object : Callback<BookResponseModel> {
            override fun onResponse(
                call: Call<BookResponseModel>,
                response: Response<BookResponseModel>
            ) {
                response
                if (response.isSuccessful) {
                    booksObservable.postValue(response.body()!!.items)
                }
            }

            override fun onFailure(call: Call<BookResponseModel>, t: Throwable) {
                //TODO ERROR HANDLING
            }

        })

        return booksObservable
    }

}