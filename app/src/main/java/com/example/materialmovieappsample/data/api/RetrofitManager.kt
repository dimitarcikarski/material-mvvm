package com.example.materialmovieappsample.data.api

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

/** Retrofit Manager class
 * class used to set Retrofit client
 */
object RetrofitManager {
    private var retrofit: Retrofit? = null

    /** Returns retrofit set with base url
     * @return Retrofit instance
     */
    fun getRetrofit(): Retrofit {
        if (retrofit != null) {
            return retrofit!!
        }
        retrofit = Retrofit.Builder()
            .baseUrl("https://www.googleapis.com/books/v1/")
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        return retrofit!!
    }
}