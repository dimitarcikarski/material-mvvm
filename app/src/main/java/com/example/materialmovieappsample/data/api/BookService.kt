package com.example.materialmovieappsample.data.api

import com.example.materialmovieappsample.data.model.BookResponseModel
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface BookService {

    @GET("volumes")
    fun healthyLifestyle(@Query("q") intitle: String): Call<BookResponseModel>

}