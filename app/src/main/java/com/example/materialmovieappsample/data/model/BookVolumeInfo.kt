package com.example.materialmovieappsample.data.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize
import java.util.ArrayList

@Parcelize
class BookVolumeInfo(
    var title: String,
    var publisher: String,
    var publishedDate: String,
    var description: String,
    var printType: String,
    var language: String,
    var previewLink: String,
    var infoLink: String,
    var averageRating: Double,
    var ratingsCount: Int,
    var authors: ArrayList<String?>,
    var imageLinks: BookImageLink
) : Parcelable