package com.example.materialmovieappsample.data.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
class BookImageLink(
    var smallThumbnail:String,
    var thumbnail:String,
) : Parcelable