package com.example.materialmovieappsample.application

import android.app.Application
import androidx.appcompat.app.AppCompatActivity
import com.example.materialmovieappsample.data.api.RetrofitManager
import com.example.materialmovieappsample.data.repository.BookRepository
import com.example.materialmovieappsample.ui.main.MainViewModel
import org.koin.android.ext.koin.androidContext
import org.koin.android.ext.koin.androidLogger
import org.koin.android.viewmodel.dsl.viewModel
import org.koin.core.context.startKoin
import org.koin.core.logger.Level
import org.koin.dsl.module

class Application  : Application(){

    override fun onCreate() {
        super.onCreate()

        /**
         * start the Koin locator dependency
         */
        startKoin {
            androidLogger(Level.ERROR)
            androidContext(this@Application)
            modules(myModule)
        }

    }

    /**
     * declaring all the fragments that we are going to use in the Koin injection
     */
    private val myModule = module {
        single { BookRepository(get()) }
        viewModel { MainViewModel(get()) }
        single { RetrofitManager }
    }
}